#include "batterysaver.h"

#include <QCoreApplication>
#include <QDBusConnection>
#include <QDBusArgument>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDBusMetaType>
#include <QDebug>
#include <QTimer>
#include <QProcess>

#define SET_PREF_COMMAND "/usr/share/ofono/scripts/set-tech-preference %1 %2"
#define DBUS_WIFI_SERVICE "com.lomiri.indicator.network"
#define DBUS_WIFI_PATH "/com/lomiri/indicator/network"
#define DBUS_WIFI_INTERFACE "org.gtk.Actions"

BatterySaver::BatterySaver(const QString &simSlot, const QString &defaultNetwork, const QString &prefferedNetwork, bool _disableWifi, QObject *parent)
    : defaultTechPrefCommand(), prefTechPrefCommand(), disableWifi(false), QObject(parent)
{

    qDebug() <<  simSlot << defaultNetwork << prefferedNetwork << _disableWifi;

    defaultTechPrefCommand = QString(SET_PREF_COMMAND).arg(simSlot, defaultNetwork);
    prefTechPrefCommand = QString(SET_PREF_COMMAND).arg(simSlot, prefferedNetwork);
    disableWifi = _disableWifi;

    qDebug() << "defaultCmd" << defaultTechPrefCommand;
    qDebug() << "prefCmd" << prefTechPrefCommand;


    bool result = QDBusConnection::sessionBus().connect(
        "com.lomiri.connectivity1",  // service
        "/com/lomiri/connectivity1/Private", // path
        "org.freedesktop.DBus.Properties",  // interface
        QStringLiteral("PropertiesChanged"), // signal
        this,
        SLOT(propertiesChanged(QString,QVariantMap,QStringList)));

    if (!result) {
        qWarning() << "error connecting dbus session";
    }
}

void BatterySaver::propertiesChanged(const QString &interface,
                                     const QVariantMap &changed,
                                     const QStringList &invalidated)

{
    bool dataEnabled = QVariant(changed["MobileDataEnabled"]).toBool();
    if (dataEnabled) {
        qDebug() << "ok dataEnabled true";
        auto& cmd = prefTechPrefCommand;
        QTimer::singleShot(1000, this, [cmd, this] () {
            QProcess process;
            process.start(cmd);
            process.waitForFinished(1000);
            toggleWifi();
        });
    } else {
        qDebug() << "ok dataEnabled false";
        auto& cmd = defaultTechPrefCommand;
        QTimer::singleShot(1000, this, [cmd] () {
            QProcess process;
            process.start(cmd);
            process.waitForFinished(1000);
        });
    }


}

void BatterySaver::toggleWifi()
{

    if (!disableWifi || !wifiEnabled()) {
        return;
    }

    QDBusMessage msg = QDBusMessage::createMethodCall(DBUS_WIFI_SERVICE, DBUS_WIFI_PATH, DBUS_WIFI_INTERFACE, QStringLiteral("Activate"));
    msg << "wifi.enable" << QVariantList() << QVariantMap();

    QDBusPendingReply<void> reply = QDBusConnection::sessionBus().call(msg);

    if (!reply.isValid())
    {
        qWarning() << "Error toggleWifiOff" << reply.error().message();
    } else {
        qDebug() << "Wifi toggled off";
    }

}

bool BatterySaver::wifiEnabled()
{
    bool wifiOn = false;

    QDBusMessage msg = QDBusMessage::createMethodCall(DBUS_WIFI_SERVICE, DBUS_WIFI_PATH, DBUS_WIFI_INTERFACE, QStringLiteral("Describe"));
    msg << "wifi.enable";
    QDBusMessage reply = QDBusConnection::sessionBus().call(msg);
    qDebug() << "dbus reply" << reply;
    if (reply.type() == QDBusMessage::ErrorMessage)
    {
        qWarning() << "Error while checking wifi state" << reply.errorMessage();
    } else {

        bool enabled;
        QDBusSignature signature;
        QVariantList values;

        const QDBusArgument &arg = reply.arguments().at(0).value<QDBusArgument>();
        if (arg.currentType() == QDBusArgument::StructureType)
        {
            arg.beginStructure();
            arg >> enabled;
            arg >> signature;
            arg >> values;
            arg.endStructure();

            wifiOn = values.at(0).toBool();

            qDebug() << "Wifi enabled? " << QString::number(wifiOn);

        } else {
            qDebug() << "no idea what to do with " << arg.currentType();
        }

   }
    return wifiOn;
}


int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    if (!QDBusConnection::sessionBus().isConnected()) {
        qWarning("Cannot connect to the D-Bus session bus.\n"
                 "Please check your system settings and try again.\n");
        return 1;
    }

    if (argc < 5) {
        qWarning("Not enough arguments.\n");
        return 1;
    }

    qDebug() << argc << "argv" << argv[1]<< argv[2]<< argv[3]<< argv[4];
    BatterySaver bs(argv[1], argv[2], argv[3], QString(argv[4]) == "true" ? true : false);

    return app.exec();
}
