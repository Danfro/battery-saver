#ifndef BATTERYSAVER_H
#define BATTERYSAVER_H

#include <QObject>

class BatterySaver: public QObject
{
    Q_OBJECT
public:
    BatterySaver(const QString &simSlot, const QString &defaultNetwork, const QString &prefferedNetwork, bool _disableWifi, QObject *parent = 0);

private Q_SLOTS:
    void propertiesChanged(const QString &interface,
                           const QVariantMap &changed,
                           const QStringList &invalidated);
private:
    void toggleWifi();
    bool wifiEnabled();

    QString defaultTechPrefCommand;
    QString prefTechPrefCommand;
    bool disableWifi;
};

#endif // BATTERYSAVER_H
