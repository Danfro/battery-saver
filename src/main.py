import subprocess
import os
from os.path import dirname, abspath

project_dir = dirname(dirname(abspath(__file__)))
home = os.path.expanduser('~')

service_name = "batterysaver"
namespace = "batterysaver.lduboeuf"

service_source_path = f'{project_dir}/service/{service_name}.service'
service_target_path = f'{home}/.config/systemd/user/{service_name}.service'
bin_source_path = f'{project_dir}/service/{service_name}'
target_path = f'{home}/.local/share/{namespace}'

def restart():

    local_bin = f'{target_path}/{service_name}'

    stop()

    # clean up local files
    if os.path.exists(local_bin):
        os.remove(local_bin)

    # copy bin to .local folder
    os.system(f'cp {bin_source_path} {local_bin}')

    # add link to service
    os.system(f'ln -s {service_source_path} {service_target_path}')

    # now ready for starting
    os.system(f'systemctl --user enable --now {service_name}')

    return status()

def status():

    status = os.system(f'systemctl is-active --user --quiet {service_name}')
    return status == 0 and os.path.islink(service_target_path) and os.path.exists(service_target_path)

def stop():
    os.system(f'systemctl --user disable --now {service_name}')
    return status()

def init():

    # we have something in systemd user folder but link is broken
    if os.path.islink(service_target_path) and not os.path.exists(service_target_path):
        return restart()

    # make sure service is up and ok, if app update, service need to be reloaded
    try:
        res = subprocess.check_output(['systemctl','--user','status',service_name], text=True);
        if "Warning:" in res or "Loaded: not-found" in res:
            return restart()
    except subprocess.CalledProcessError as e:
        stop()
        return False

    return True
